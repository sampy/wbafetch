#!/usr/bin/python3
#
# @uthor sampy
#
# DESCRIPTION: Create fetch
# CREATION: Jun 2019
# REQUIREMENTS:
#IMPORTS
import pyfiglet
import subprocess
import colorama
import argparse
import os

#VARS
desc = "Vitamined MOTD" 
parser=argparse.ArgumentParser(description=desc)

RED='\033[31m'
GREEN='\033[32m'
YELLOW='\033[33m'
BLUE='\033[34m'
WHITE='\033[37m'
PURPLE='\033[35m'
CLEAR='\033[2J'

VERSION="0.33AH"

#FUNCTIONS
def get_version():
    return print('Version of wbafetch:',VERSION)

def get_servername():
    servername=subprocess.run(['hostname'], stdout=subprocess.PIPE).stdout.decode('utf-8')
    return servername

def get_uptime():
    uptime=subprocess.run(['uptime'], stdout=subprocess.PIPE).stdout.decode('utf-8')
    return uptime

def get_warningUptime():
    value=get_uptime()
    uptime=(value.split())
    if uptime[3] == "days,":
        if int(uptime[2]) > 200:
            return("Too many uptime days!!!")
    return ""

def get_diskspace():
    dfcommand=subprocess.run(['df', '-h'], stdout=subprocess.PIPE).stdout.decode('utf-8')
    diskusages = dfcommand.split("\n",1)[1];
    #title = dfcommand.split("\n",1)[0];
    #output_console(WHITE,title)
    output_console(WHITE,dfcommand.splitlines()[0]) ## TITLE

    for elements in diskusages.splitlines():
        used=elements.split()[4]
        if int(used.split('%')[0]) >= 90:
            output_console(RED,elements)
        elif int(used.split('%')[0]) >= 70:
            output_console(YELLOW,elements)
        else:
            output_console(GREEN,elements)

def get_routes():
    routes=subprocess.run(['ip', 'r'], stdout=subprocess.PIPE).stdout.decode('utf-8')
    return routes

def process_print(service_name):
    status = os.system('systemctl status '+service_name+'> /dev/null')
    if status == 0:
        output_console(GREEN,"Process: "+service_name+ " RUNNING")
    else:
        output_console(RED,"Process: "+service_name+ " STOPPED")

def get_liferayVersion():
    referencefile = open("/opt/liferay/tomcat/webapps/java-hook/WEB-INF/liferay-plugin-package.properties", "rt")
    contents = referencefile.read()
    return contents 

def printer(element):
    print(pyfiglet.figlet_format(element, font = "big"))

def output_console(color,message):
    return print(color + message)

def run_main():
    printer(get_servername())
    output_console(BLUE,get_uptime())
    output_console(PURPLE,get_warningUptime())
    print("")
    get_diskspace()
    output_console(BLUE,get_routes())
    output_console(WHITE,"")
    

#MAIN

parser.add_argument('-v', '--version', action='store_true', help='Show version')
parser.add_argument('-r', '--run', action='store_true', help='Run')
parser.add_argument('-p', '--process', type=str, help='Check process status')
parser.add_argument('-l', '--liferay', action='store_true', help='Check LifeRay versions')


args = parser.parse_args()

if args.version: 
    get_version()

if args.run:
    run_main()

if args.process:
    for elements in args.process.split(','):
        if elements == "liferay":
            output_console(PURPLE,get_liferayVersion().splitlines()[0])
            output_console(PURPLE,get_liferayVersion().splitlines()[1])
            output_console(PURPLE,get_liferayVersion().splitlines()[5])
        process_print(elements)
        output_console(WHITE,"")

if args.liferay:
    output_console(PURPLE,get_liferayVersion().splitlines()[0])
    output_console(PURPLE,get_liferayVersion().splitlines()[1])
    output_console(PURPLE,get_liferayVersion().splitlines()[5])
    output_console(WHITE,"")
    