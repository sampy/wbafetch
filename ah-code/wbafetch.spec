# -*- mode: python -*-

block_cipher = None


a = Analysis(['wbafetch.py'],
             pathex=['/home/sampy/workdir/wbafetch/code'],
             binaries=[],
             datas=[('/home/sampy/.local/lib/python3.5/site-packages/pyfiglet', 'pyfiglet')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='wbafetch',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
