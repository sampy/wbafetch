# wbafetch

Before that project I made this in bash changing motd, but finally I tried with python and deploy to all my servers. It's not a very nice project, but for me is usefull and I started to learn Python doing that.

## Usage

wbafetch.py -r

* shows the information about disks,uptime,hostname

wbafetch.py -p process_name1,process_name2,..

* shows status of the process

wbafetch.py -v 

* shows version.
